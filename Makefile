OTHER_FLAGS=-Wall -Wextra -Wpedantic -std=c99
DEBUG_FLAGS=-O0 -g
RELEASE_FLAGS=-O2 -s -DNDEBUG

all: debug

debug: bin/dserver bin/dclient

release: bin/server bin/client

clear:
	rm -r bin

bin/dserver: server.c
	mkdir bin 2>/dev/null || true
	$(CC) $(CC_FLAGS) $(OTHER_FLAGS) $(DEBUG_FLAGS) -o $@ $<

bin/dclient: client.c
	mkdir bin 2>/dev/null || true
	$(CC) $(CC_FLAGS) $(OTHER_FLAGS) $(DEBUG_FLAGS) -o $@ $<

bin/server: server.c
	mkdir bin 2>/dev/null || true
	$(CC) $(CC_FLAGS) $(OTHER_FLAGS) $(RELEASE_FLAGS) -o $@ $<

bin/client: client.c
	mkdir bin 2>/dev/null || true
	$(CC) $(CC_FLAGS) $(OTHER_FLAGS) $(RELEASE_FLAGS) -o $@ $<

.PHONY: all debug release clear

