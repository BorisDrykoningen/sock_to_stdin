# sock\_to\_stdin

Reads a stream of data from sockets, write it on a child's process input

## How to build

```
make release
```

## How to use

```
bin/server path/to/an/unix/socket command arg1 arg2 arg3
```

```
bin/client path/to/an/unix/socket 'Data to send on stdin'
```

## Will this software be updated

Kinda

