#define _DEFAULT_SOURCE
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv) {
    int status = EXIT_FAILURE;
    if (argc != 3) {
        fprintf(stderr, "Expected exactly 2 arguments, got %d\n", argc - 1);
        return EXIT_FAILURE;
    }

    const size_t path_len = strlen(argv[1]);
    const socklen_t addr_len = offsetof(struct sockaddr_un, sun_path) + path_len;
    struct sockaddr_un addr;
    if (path_len > sizeof(addr.sun_path)) {
        fprintf(
            stderr,
            "Your operating system supports paths of length %zu at most. "
            "You submitted a path of length %zu\n",
            sizeof(addr.sun_path),
            path_len
        );
        goto socket_error;
    }
    addr.sun_family = AF_UNIX;
    // The trailing '\0' is skipped
    memcpy(&addr.sun_path, argv[1], path_len);

    const int server = socket(AF_UNIX, SOCK_STREAM, 0);
    if (server < 0) {
        perror("[ERROR] Couldn't create a socket");
        goto socket_error;
    }

    if (connect(server, (struct sockaddr*) &addr, addr_len) < 0) {
        perror("[ERROR] Couldn't connect to the server");
        goto connect_error;
    }

    const size_t msg_size = strlen(argv[2]);
    size_t sent = 0;
    while (sent < msg_size) {
        const ssize_t this_time = send(server, argv[2] + sent, msg_size - sent, MSG_NOSIGNAL);
        if (this_time < 0) {
            if (errno == EINTR) {
                continue;
            }
            perror("[ERROR] send error");
            goto send_error;
        }
        sent += this_time;
    }

    status = EXIT_SUCCESS;

send_error:
    shutdown(server, SHUT_RDWR);
connect_error:
    if (close(server) < 0) {
        perror("[WARNING] Couldn't close connexion to the server");
    }
socket_error:

    return status;
}

