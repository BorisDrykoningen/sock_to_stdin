#define _DEFAULT_SOURCE
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef BACKLOG
#define BACKLOG 128
#endif

#ifndef BLOCK_SIZE
#define BLOCK_SIZE 16
#endif


pid_t spawn_piped(int ipc[2], char** cmd);
int forward_inputs(int listener, int output);

volatile int child_alive = 1;
void on_child_died(int);


int main(int argc, char** argv) {
    int status = EXIT_FAILURE;
    if (argc < 3) {
        fprintf(stderr, "Expected at least 2 arguments, got %d\n", argc - 1);
        return EXIT_FAILURE;
    }

    // First, ignore SIGPIPE to avoid returning a non-zero exit status
    struct sigaction sig_pipe;
    sig_pipe.sa_handler = SIG_IGN;
    sigemptyset(&sig_pipe.sa_mask);
    sig_pipe.sa_flags = 0;
    if (sigaction(SIGPIPE, &sig_pipe, NULL) < 0) {
        // This is not a fatal error
        perror("[WARNING] Can't ignore SIGPIPE. This will work anyway, but the exit code will report an error");
    }

    struct sigaction sig_chld;
    sig_chld.sa_handler = on_child_died;
    sigemptyset(&sig_chld.sa_mask);
    sig_chld.sa_flags = 0;
    if (sigaction(SIGCHLD, &sig_chld, NULL) < 0) {
        // This is not a fatal error
        perror("[WARNING] Can't handle SIGCHLD. This may not properly terminate");
    }

    // Then, spawn a process behind a pipe
    int ipc[2];
    if (pipe(ipc) < 0) {
        perror("[ERROR] Can't establish connexion between processes");
        goto pipe_error;
    }

    const pid_t child = spawn_piped(ipc, argv + 2);
    if (child < 0) {
        perror("Couldn't spawn a child process");
        goto spawn_error;
    }

    // Then, the network stuff
    const int in_sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (in_sock < 0) {
        perror("Couldn't create a socket");
        goto socket_error;
    }

    // Remove any previously existing file
    if (unlink(argv[1]) < 0 && errno != ENOENT) {
        perror("Couldn't remove previously-existing file");
    }

    const size_t path_len = strlen(argv[1]);
    struct sockaddr_un in_sock_addr;
    in_sock_addr.sun_family = AF_UNIX;
    if (path_len > sizeof(in_sock_addr.sun_path)) {
        fprintf(
            stderr,
            "Your operating system supports paths of length %zu at most. "
            "You submitted a path of length %zu\n",
            sizeof(in_sock_addr.sun_path),
            path_len
        );
        goto bind_error;
    }
    // The '\0' is not copied because it is not needed for unix socket paths
    memcpy(in_sock_addr.sun_path, argv[1], path_len);
    const socklen_t in_sock_len = offsetof(struct sockaddr_un, sun_path) + path_len;
    if (bind(in_sock, (struct sockaddr*) &in_sock_addr, in_sock_len) < 0) {
        perror("[ERROR] Couldn't bind the socket to the requested path");
        goto bind_error;
    }

    // One connexion at a time is expected. But a backlog of size 1 wouldn't be
    // desirable, because scripts may fail when another script is talking to the
    // child process. However, we can't afford to treat commands asynchronously
    // to prevent output from overlapping, so, all we've got is the backlog
    if (listen(in_sock, BACKLOG) < 0) {
        perror("[WARNING] Couldn't listen");
        goto listen_error;
    }

    status = forward_inputs(in_sock, ipc[1]) == 0 ? EXIT_SUCCESS : EXIT_FAILURE;

listen_error:
    if (shutdown(in_sock, SHUT_RDWR) < 0) {
        perror("[WARNING] Couldn't shutdown socket");
    }
    if (unlink(argv[1]) < 0) {
        perror("[WARNING] Couldn't delete the socket");
    }
bind_error:
    if (close(in_sock) < 0) {
        perror("[WARNING] Couldn't close socket");
    }
socket_error:
spawn_error:
    if (close(ipc[1]) < 0) {
        perror("[WARNING] Couldn't close pipe with child process");
    }
pipe_error:

    return status;
}


pid_t spawn_piped(int ipc[2], char** cmd) {
    const pid_t child = fork();
    if (child < 0) {
        perror("[ERROR] Couldn't create a child process");
        return -1;
    }

    if (child == 0) {
        if (close(ipc[1])) {
            perror("[WARNING] Couldn't close writting end of pipe");
        }
        if (dup2(ipc[0], 0) < 0) {
            perror("[ERROR] Couldn't bind pipe to stdin");
            exit(EXIT_FAILURE);
        }
        if (close(ipc[0]) < 0) {
            perror("[WARNING] Couldn't close reading end of pipe");
        }
        execvp(cmd[0], cmd);
        exit(EXIT_FAILURE);
    }

    if (close(ipc[0])) {
        perror("[WARNING] Couldn't close reading end of pipe");
    }
    return child;
}


int forward_inputs(int listener, int output) {
    while (child_alive) {
        const int client = accept(listener, NULL, NULL);
        if (client < 0) {
            perror("[WARNING] accept failed");
            continue;
        }

        char buff[BLOCK_SIZE];
        ssize_t buff_read;
        while ((buff_read = recv(client, buff, sizeof(buff), 0)) > 0) {
            if (write(output, buff, buff_read) < 0) {
                if (errno == EPIPE) {
                    // Broken pipe means the child process died or closed its
                    // standard input. We are done
                    if (close(client) < 0) {
                        perror("[WARNING] Couldn't close socket");
                    }
                    return 0;
                }
                perror("[WARNING] write error");
                break;
            }
        }
        if (buff_read < 0) {
            perror("[WARNING] Connexion unexpectedly closed");
        }

        if (close(client)) {
            perror("[WARNING] Couldn't close socket");
        }
    }

    return 0;
}


void on_child_died(int unused) {
    (void) unused;
    child_alive = 0;
}

